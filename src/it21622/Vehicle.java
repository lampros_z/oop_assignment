
package it21622;

/**
 * This class has all the fields required to keep the state of a Vehicle object.
 * @author it21622
 * 
 */
public abstract class Vehicle {
    /**
     * The id number of the vehicle object.
     */
    private int id;
    /**
     * The type of fuel the vehicle supports(Gasoline, petrol, LNG).
     */
    private String fuel;
    /**
     * Engine's horsepower.
     */
    private double horsepower;
    /**
     * The size of each wheel of the vehicle.
     */
    private double wheelSize;
    /**
     * The engine displacement of the vehicle.
     */
    private double engineDiscplacement;
    /**
     * The amount of money the vehicle costs each hour the vehicle is rented.
     */
    private double cost;
    /**
     * A value indicating whether the vehicle is rented or not and hence if it is available for rent or not.
     */
    private boolean reserved;

    public Vehicle(int id, String fuel, double horsepower, double wheelSize, double engineDiscplacement, double cost) {
        this.id = id;
        this.fuel = fuel;
        this.horsepower = horsepower;
        this.wheelSize = wheelSize;
        this.engineDiscplacement = engineDiscplacement;
        this.cost = cost;
        this.reserved = false;
    }
    
    public int getId(){
        return id;
    }
    
    public void setId(int id){
        this.id = id;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public double getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(double horsepower) {
        this.horsepower = horsepower;
    }

    public double getWheelSize() {
        return wheelSize;
    }

    public void setWheelSize(double wheelSize) {
        this.wheelSize = wheelSize;
    }

    public double getEngineDiscplacement() {
        return engineDiscplacement;
    }

    public void setEngineDiscplacement(double engineDiscplacement) {
        this.engineDiscplacement = engineDiscplacement;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }
    
    @Override
    public String toString() {
        return "Vehicle{" + "fuel=" + fuel + ", horsepower=" + horsepower + ", wheelSize=" + wheelSize + ", engineDiscplacement=" + engineDiscplacement + ", cost=" + cost + '}';
    }
    /**
     * A method that prints a formatted string to the screen.
     * @return a formatted string
     */
    public abstract String printHeader();
    /**
     * A method that will print the vehicle details formatted to the screen.
     */
    public abstract void printVehicleDetails();
    /**
     * A method that prints a formatted string to the screen.
     * @return a formatted string.
     */
    public abstract String printFooter();
    
    
}
