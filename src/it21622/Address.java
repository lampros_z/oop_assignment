
package it21622;

/**
 * A class containing all the necessary information for a valid street address.
 * @author it21622
 */
public class Address {
    
    private String streetAddress;
    private String postcode;
    private String town;
    private String country;
    
    public Address(String streetAddress, String postcode, String town, String country) {
        this.streetAddress = streetAddress;
        this.postcode = postcode;
        this.town = town;
        this.country = country;
    }
    
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }
    
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
    
    public void setTown(String town) {
        this.town = town;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
    public String getStreetAddress() {
        return streetAddress;
    }
    
    public String getPostcode() {
        return postcode;
    }
    
    public String getTown() {
        return town;
    }
    
    public String getCountry() {
        return country;
    }
    
    @Override
    public String toString() {
        return "Address{" + "streetAddress=" + streetAddress + ", postcode=" + postcode + ", town=" + town + ", country=" + country + '}';
    }
    
}
