
package it21622;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.ChronoUnit.MINUTES;

/**
 * A RentalReservation is the basic building block of our BookingSystem. In particular, when someone rents a car, a rental reservation object
 * containing all the required information is created. Information such as user's personal info, car's details and credit cards.
 * @author it21622
 */
public class RentalReservation {
    /**
     * a static field denoting the credit card payment method.
     */
    public static final String CREDIT_CARD = "Credit card";
    /**
     * a static field denoting cash as a payment method.
     */
    public static final String CASH = "Cash";
    /**
     * A unique identifier for the rental reservation object.
     */
    private int id;
    private final DateTimeFormatter dateTimeFormatter;
    /**
     * This field holds the rent date and time of a vehicle.
     */
    private LocalDateTime rentDateTime;
   /**
     * This field holds the return date and time of a vehicle.
     */
    private LocalDateTime returnDateTime;
    /**
     * The total cost of the rental reservation.
     */
    private double cost;
    /**
     * The payment method that the user has selected.
     */
    private String paymentMethod;
    /**
     * The vehicle that the user has selected.
     */
    private Vehicle vehicle;
    /**
     * The user to whom this rental reservation belongs to.
     */
    private User user;
    /**
     * The pick-up point of the vehicle.
     */
    private Store deliveryPoint;
    /**
     * The store where the user will return the vehicle.
     */
    private Store returnPoint;
    
    public RentalReservation(int id, String rentDate, String rentHours, String returnDate, String returnHours, String paymentMethod, Vehicle vehicle, User user, Store deliveryPoint, Store returnPoint) {
        this.id = id;
        this.dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        this.rentDateTime = LocalDateTime.parse( rentDate+" "+ rentHours, dateTimeFormatter);
        this.returnDateTime = LocalDateTime.parse( returnDate+" "+ returnHours, dateTimeFormatter);
        this.paymentMethod = paymentMethod;
        this.vehicle = vehicle;
        this.user = user;
        this.deliveryPoint = deliveryPoint;
        this.returnPoint = returnPoint;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public double getCost() {
        return cost;
    }
    
    public String getPaymentMethod() {
        return paymentMethod;
    }
    
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    public Vehicle getVehicle() {
        return vehicle;
    }
    
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public LocalDateTime getRentDateTime() {
        return rentDateTime;
    }
    
    public LocalDateTime getReturnDateTime() {
        return returnDateTime;
    }
    
    public void setRentDateTime(String date, String hour) {
        this.rentDateTime = LocalDateTime.parse( date+" "+ hour, dateTimeFormatter);
    }
    
    public void setReturnDateTime(String date, String hour) {
        this.returnDateTime = LocalDateTime.parse( date+" "+ hour, dateTimeFormatter);
    }
    
    
    public void reserveVehicle(){
        vehicle.setReserved(true);
    }
    
    public void returnVehicle(){
        vehicle.setReserved(false);
    }
    
    public boolean isReserved(){
        return vehicle.isReserved();
    }
    
    public Store getDeliveryPoint() {
        return deliveryPoint;
    }
    
    public void setDeliveryPoint(Store deliveryPoint) {
        this.deliveryPoint = deliveryPoint;
    }
    
    public Store getReturnPoint() {
        return returnPoint;
    }
    
    public void setReturnPoint(Store returnPoint) {
        this.returnPoint = returnPoint;
    }
    
    @Override
    public String toString() {
        return "RentalReservation{" + "id=" + id + ", rentDateTime=" + rentDateTime + ", returnDateTime=" + returnDateTime + ", cost=" + cost + ", paymentMethod=" + paymentMethod + ", vehicle=" + vehicle.toString() + ", user=" + user.toString() + ", deliveryPoint=" + deliveryPoint.toString() + ", returnPoint=" + returnPoint.toString() + '}';
    }
    /**
     * A method for printing the details of the rental reservation formatted.
     */
    public void printRentalReservation(){
        System.out.printf("+--------+------------+------------------+--------------+------------+---------------+------------------------+------------------+--------------+--------------------------+--------------------------+------------------------+--------------------+--------------------+------------------------+------------------------+\n");
        System.out.printf("| ID     | Cost       | Payment Method   | Vehicle Type | Fuel       | Horsepower    | Engine Displacement    | Luggage Space    | AMKA         | First Name               | Last Name                | Credit card            | Rent date          | Return date        | Pick-up point address  | Return point address   |\n");
        System.out.printf("+--------+------------+------------------+--------------+------------+---------------+------------------------+------------------+--------------+--------------------------+--------------------------+------------------------+--------------------+--------------------+------------------------+------------------------+\n");
        Car car;
        if(vehicle instanceof Car){
            car = (Car)vehicle;
            System.out.printf("| %-6d |\u20AC%10.2f | %-16s | %-12s | %-10s | %-13.2f | %-22.2f | %-2d               | %-11s  | %-24s | %-24s | %-22s | %-18s | %-18s | %-22s | %-22s |%n", id,cost, paymentMethod, "Car", vehicle.getFuel(), vehicle.getHorsepower(), vehicle.getEngineDiscplacement(), car.getLuggageSpace(), user.getAMKA(), user.getFirstName(), user.getLastName(), user.getCreditCardNumber(), rentDateTime.format(dateTimeFormatter), returnDateTime.format(dateTimeFormatter), deliveryPoint.getAddress().getStreetAddress(), returnPoint.getAddress().getStreetAddress());
        }
        else if(vehicle instanceof TwoWheeler)
            System.out.printf("| %-6d |\u20AC%10.2f | %-16s | %-12s | %-10s | %-13.2f | %-22.2f | %-2d               | %-11s  | %-24s | %-24s | %-22s | %-18s | %-18s | %-22s | %-22s |%n", id,cost, paymentMethod, "Two wheeler", vehicle.getFuel(), vehicle.getHorsepower(), vehicle.getEngineDiscplacement(), 0, user.getAMKA(), user.getFirstName(), user.getLastName(), user.getCreditCardNumber(), rentDateTime.format(dateTimeFormatter), returnDateTime.format(dateTimeFormatter), deliveryPoint.getAddress().getStreetAddress(), returnPoint.getAddress().getStreetAddress());

        System.out.printf("+--------+------------+------------------+--------------+------------+---------------+------------------------+------------------+--------------+--------------------------+--------------------------+------------------------+--------------------+--------------------+------------------------+------------------------+\n");
        
    }    
    /**
     * This method calculates the total cost of the rental reservation and rounds the result to two decimal digits.
     * @return the total cost of a rental reservation
     */
    public double calculateRentalReservationCost(){
        int reservationMinutes = (int) rentDateTime.until(returnDateTime, MINUTES);
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.UP);
        cost = Double.parseDouble(df.format(vehicle.getCost() * ( reservationMinutes / 60.0 )));
        return cost;
    }
    
}

