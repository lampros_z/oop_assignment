
package it21622;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DataManager class is responsible for creating some basic data structures that will supply with data the BookingSystem
 * The basic entities are Stores, Cars, Two-Wheelers, Rental Reservations
 * @author it21622
 */
public class DataManager {
    private List<Store> stores;
    private List<Vehicle> cars;
    private List<Vehicle> twoWheelers;
    private Map <Integer, RentalReservation> rentalReservations;
    private Map <String, User> users;
    public DataManager() {
        this.rentalReservations = new HashMap<>();
        this.twoWheelers = new ArrayList<>();
        this.cars = new ArrayList<>();
        this.stores = new ArrayList<>();
        this.users = new HashMap<>();
        stores.add( new Store (1, Store.SIMPLE_STORE, new Address("Ανθέων 32", "ΤΚ43456",  "Αθήνα", "Ελλάδα")));
        stores.add( new Store (2, Store.SIMPLE_STORE, new Address("Σωκράτους 58", "ΤΚ13426",  "Αθήνα", "Ελλάδα")));
        stores.add( new Store (3, Store.PICKUP_POINT, new Address("Παπαδιαμαντοπούλου 20", "ΤΚ23872",  "Αθήνα", "Ελλάδα")));
        stores.add( new Store (4, Store.PICKUP_POINT, new Address("Παπάγου 3", "ΤΚ33421",  "Αθήνα", "Ελλάδα")));
        Car car2 = new Car(2, "Βενζίνη", 89, 15, 1125, 2.6, 2, 2, 3);
        Car car1 =  new Car(1, "Βενζίνη", 156, 17, 1499, 3.5, 5, 4, 5);
        cars.add(car1);
        cars.add(car2);
        cars.add(new Car(3, "Βενζίνη", 200, 18, 2600, 4.7, 7, 4, 8));
        cars.add(new Car(4, "Βενζίνη", 205, 18, 2700, 4.7, 7, 4, 8));
        cars.add(new Car(5, "Βενζίνη", 210, 18, 2650, 4.7, 7, 4, 8));
        cars.add(new Car(6, "Βενζίνη", 198, 18, 2670, 4.7, 7, 4, 8));
        User user1 = new User(1, "21079812345", "Lampros", "Zouloumis" ,"AK 547896", "0123456789", 21, "6978542354", null);
        twoWheelers.add(new TwoWheeler(1, "Βενζίνη", 105, 15, 998, 3.1, false, true));
        twoWheelers.add(new TwoWheeler(2, "Ηλεκτρικό", 147, 15, 982, 2.4, true, true));
        twoWheelers.add(new TwoWheeler(3, "Βενζίνη", 55, 14, 115, 1.4, false, true));
        twoWheelers.add(new TwoWheeler(4, "Βενζίνη", 85, 16, 478, 2.9, true, false));
        twoWheelers.add(new TwoWheeler(5, "Ηλεκτρικό", 180, 14, 950, 2.8, false, true));
        RentalReservation rentalReservation = new RentalReservation(1, "21/03/2020", "10:00", "22/03/2020", "10:00", RentalReservation.CASH, car2, user1, stores.get(0), stores.get(0));
        RentalReservation rentalReservation2 = new RentalReservation(2, "22/03/2020", "10:00", "23/03/2020", "10:00", RentalReservation.CASH, car1, user1, stores.get(0), stores.get(0));
        rentalReservation.reserveVehicle();
        rentalReservation2.reserveVehicle();
        rentalReservation2.calculateRentalReservationCost();
        rentalReservation.calculateRentalReservationCost();
        rentalReservations.put(1, rentalReservation);
        rentalReservations.put(2, rentalReservation2);
        this.users.put(user1.getAMKA(), user1);

    }
    
    public List<Store> getStores() {
        return stores;
    }
    
    public List<Vehicle> getCars() {
        return cars;
    }
    
    public List<Vehicle> getTwoWheelers() {
        return twoWheelers;
    }

    public Map<Integer, RentalReservation> getRentalReservations() {
        return rentalReservations;
    }

    public Map<String, User> getUsers() {
        return users;
    }
    
}
