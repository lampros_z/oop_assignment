
package it21622;

/**
 * A class containing all the necessary information required for maintaining the state of a user in our Booking System. 
 * @author it21622
 */
public class User {
    /**
     * The id number of the user.
     */
    private int id;
    /**
     * The AMKA(unique identifier) of the user.
     */
    private String AMKA;
    /**
     * User's first name.
     */
    private String firstName;
    /**
     * User's last name.
     */
    private String lastName;
    /**
     * User's identity number.
     */
    private String identityNumber;
    /**
     * User's drivers license number.
     */
    private String driversLicenceNumber;
    /**
     * User's age.
     */
    private int age;
    /**
     * User's phone number.
     */
    private String phoneNumber;
    /**
     * User's credit card number.
     */
    private String creditCardNumber;

    public User(int id, String AMKA, String firstName, String lastName, String identityNumber, String driversLicenceNumber, int age, String phoneNumber, String creditCardNumber) {
        this.id = id;
        this.AMKA = AMKA;
        this.firstName = firstName;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.driversLicenceNumber = driversLicenceNumber;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.creditCardNumber = creditCardNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getDriversLicenceNumber() {
        return driversLicenceNumber;
    }

    public void setDriversLicenceNumber(String driversLicenceNumber) {
        this.driversLicenceNumber = driversLicenceNumber;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getAMKA() {
        return AMKA;
    }

    public void setAMKA(String AMKA) {
        this.AMKA = AMKA;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    @Override
    public String toString() {
        return "User{" + "id=" + id + ", AMKA=" + AMKA + ", identityNumber=" + identityNumber + ", driversLicenceNumber=" + driversLicenceNumber + ", age=" + age + ", phoneNumber=" + phoneNumber + ", creditCardNumber=" + creditCardNumber + '}';
    }


    
    
    
}
