package it21622;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MINUTES;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * BookingSystem class models the functionality of the BookingSystem combining all other classes.
 * Some additional fields support the operation of the System.
 *
 * @author it21622
 *
 */
public class BookingSystem{
    /**
     * A static field holding the number of users in the system during the run time.
     * Every time a user is registered to the system the value of this field is incremented by one.
     */
    private static int USER_COUNTER = 0;
    /**
     * Holds the number of rental reservations in the system. When a rental reservation is created the value of this static field
     * is incremented by one. When a rental reservation is canceled and hence removed from the system the value of this field
     * is not affected.
     */
    private static int RENTAL_RESERVATION = 0;
    /**
     * A static field that keeps the number of available vehicles in the system. In particular, when someone rents a vehicle the
     * vehicle is reserved and hence the value of this field is decreased by one.
     */
    private static int AVAILABLE_VEHICLES = 0;
    private final Pattern datePattern;
    private final Pattern hoursPattern;
    private final Scanner scanner;
    /**
     * A list containing all the stores.
     */
    private final List<Store> stores;
    /**
     * A list containing all the TwoWheelers of the company.
     */
    private final List<Vehicle> twoWheelers;
     /**
     * A list containing all the cars that the company has.
     */
    private final List<Vehicle> cars;
        /**
     * Contains mappings between an integer index and a linked list that can store vehicles.
     * This field is used during the run time of the program, to return according to the users choice(index)
     * a list containing either car objects or two-wheeler objects.
     */
    private final Map<Integer, List<Vehicle>> vehicles;
     /**
     * Contains mappings between the AMKA (unique identifier) of the user and its rental reservations.
     * That means a user can have many rental reservations and a rental reservation can belong only to one user.
     * In other words, this way a one-to-many mapping between a user and its rental reservations is achieved.
     */
    private Map<String, List<RentalReservation>> userRentalReservations;
    /**
     * Contains mappings between the id of  a rental reservation and the rental reservation 
     * objects itself, hence the the naming rentalReservationRegistry.
     */ 
    private Map<Integer, RentalReservation> rentalReservationsRegistry;
    /**
     * Contains mappings between the AMKA of a user and the user's object.
     * This field is used to track whether a user is already stored in the system.
     * If the user is already registered in this registry, its data is obtained.
     * This way the user can skip the part of entering personal information details and proceed immediately to the payment process.
     */ 
    private Map<String, User> userRegistry;
    
   /**
     * Basic constructor of this class. When constructor's invocation is successfully completed, the system will have all the necessary
     * information to function properly and serve the customers.
     * @param stores A list containing the stores of the company.
     * @param twoWheelers A list containing the two-wheelers of the company.
     * @param cars A list containing the cars of the company.
     * @param rentalReservations A "dictionary" having some saved instances of rental reservations.
     * @param users A "dictionary" containing users data
     */ 
    public BookingSystem(List<Store> stores, List<Vehicle> twoWheelers, List<Vehicle> cars, Map<Integer, RentalReservation> rentalReservations, Map<String, User> users) {
        this.datePattern = Pattern.compile("^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$");
        this.hoursPattern = Pattern.compile("([01]{1,2}[0-9]|2[0-3]):[0-5][0-9]");
        this.scanner = new Scanner(System.in);
        this.stores = stores;
        this.twoWheelers = twoWheelers;
        this.cars = cars;
        this.vehicles = new HashMap<>();
        this.userRentalReservations = new HashMap<>();
        this.userRegistry = users;
        this.rentalReservationsRegistry = rentalReservations;
        this.rentalReservationsRegistry.values().forEach((rentalReservation) -> {
            assignRentalReservationsToUser(this.userRentalReservations, rentalReservation);
            if( rentalReservation.getUser() != null )
                USER_COUNTER++;
        });
        RENTAL_RESERVATION = rentalReservations.size();
        AVAILABLE_VEHICLES = twoWheelers.size() + cars.size();
        this.vehicles.put(1, cars);
        this.vehicles.put(2, twoWheelers);
    }
    /**
     * This method "starts" the booking system.
     * It is responsible for two main things.
     * 1) Present a menu with four options to the user.
     * 	This way the user can interact with the system and possibly change its internal state.
     * 2) According to the user's requested service, this method invokes the appropriate method to handle the task and serve the user.
     */ 
    public void start(){
        
        boolean run = true;
        int choice;
        do{
            System.out.println("|----------------------------------------------------|");
            System.out.println("|           Welcome to \"JAVIS rent a motor\"          |");
            System.out.println("|----------------------------------------------------|");
            System.out.println("|           1:  Rent a vehicle                       |");
            System.out.println("|           2:  Update a rental reservation          |");
            System.out.println("|           3:  View rental reservation details      |");
            System.out.println("|           4:  Quit                                 |");
            System.out.println("|----------------------------------------------------|");
            System.out.print("Please select an option from the menu above: ");
            try{
                choice = Integer.parseInt(scanner.nextLine());
            } catch(NumberFormatException e){
                choice = -1;
            }
            switch(choice){
                case 1:
                    RentalReservation rentalReservation = rentVehicle();
                    if(rentalReservation != null){
                        rentalReservation.reserveVehicle();
                        AVAILABLE_VEHICLES--;
                        rentalReservation.calculateRentalReservationCost();
                        rentalReservationsRegistry.put(rentalReservation.getId(), rentalReservation);
                        assignRentalReservationsToUser(userRentalReservations, rentalReservation);
                        rentalReservation.printRentalReservation();
                    }
                    break;
                    
                case 2:
                    updateRentalReservation();
                    System.out.println("");
                    break;
                case 3:
                    viewRentalReservationDetails();
                    System.out.println("");
                    break;
                case 4:
                    run = false;
                    break;
                default:
                    System.out.println("Please enter a valid input!");
                    break;
            }
        } while(run);
        
    }
   /**
     * This method provides the functionality for renting a vehicle.
     * If a user requested to rent a vehicle then this method is invoked.
     * Messages are printed to the screen so as to prompt the user to provide input.
     * It protects the system from crashing by validating the user's input.
     * Finally, creates an object containing all the information that the user has entered, 
     * updates the internal state of the system(static fields)
     * and return a rental reservation object to the caller method.
     * @return A RentalReservation object
     */ 
    private RentalReservation rentVehicle(){
        if( AVAILABLE_VEHICLES > 0 ){
            boolean canRent = false;
            int availableVehicles = 0;
            System.out.println("");
            int choice = -1;
            do{
                System.out.println("Rent a vehicle service");
                System.out.println("_____________________");
                System.out.println("1: Car");
                System.out.println("2: Two-wheeler");
                System.out.println("3: Exit");
                System.out.println("");
                do {
                    try{
                        choice = Integer.parseInt(scanner.nextLine());
                    }catch(NumberFormatException e){
                        choice = -1;
                    }
                } while( choice < 1 || choice > 3 );
                if( choice == 3 )
                    return null;
                for( Vehicle vehicle : vehicles.get(choice)){
                    if(!vehicle.isReserved())
                        availableVehicles++;
                }
                if(availableVehicles > 0)
                    canRent = true;
                else
                    System.out.println("No vehicles of the specified type are available, please selct another type or contact with the store.");
            }while(!canRent);
            int vehicleChoice = -1;
            int storeChoice = -1;
            //**************************
            Vehicle vehicle;
            String rentDate;
            String rentHour;
            String returnDate;
            String returnHour;
            Store deliveryPoint;
            Store returnPoint;
            //**************************
            Set<Integer> validationSet = new HashSet<>();
            Map<Integer, Integer> indexMapings = new HashMap<>();
            validationSet.clear();
            List<Vehicle> vehiclesSelected = vehicles.get(choice);
            System.out.format(vehicles.get(choice).get(0).printHeader());
            for(int i = 0; i < vehiclesSelected.size(); i++){
                if(vehiclesSelected.get(i).isReserved())
                    continue;
                System.out.print(vehiclesSelected.get(i).toString());
                validationSet.add(vehiclesSelected.get(i).getId());
                indexMapings.put(vehiclesSelected.get(i).getId(), i);
            }
            System.out.format(vehicles.get(choice).get(0).printFooter());
            do{
                try{
                    System.out.print("Select vehicle(ID)? ");
                    vehicleChoice = Integer.parseInt(scanner.nextLine());
                }catch(NumberFormatException e){
                    vehicleChoice = -1;
                }
                if( validationSet.contains(vehicleChoice) )
                    break;
            } while(true);
            
            vehicle = vehiclesSelected.get( indexMapings.get(vehicleChoice) );
            
            rentDate = getRentDate();
            
            rentHour = getRentHours( rentDate );
            
            returnDate = getReturnDate(rentDate);
            
            returnHour = getReturnHour(rentDate, returnDate, rentHour);
            
            validationSet.clear();
            indexMapings.clear();
            System.out.format(stores.get(0).printHeader());
            for(int i = 0 ; i < stores.size(); i++){
                System.out.print(stores.get(i).toString());
                validationSet.add(stores.get(i).getId());
                indexMapings.put(stores.get(i).getId(), i);
            }
            System.out.format(stores.get(0).printFooter());
            do{
                try{
                    System.out.print("Select a delivery point from the list above? ");
                    storeChoice = Integer.parseInt(scanner.nextLine());
                }catch(NumberFormatException e){
                    storeChoice = -1;
                }
                if(validationSet.contains(storeChoice))
                    break;
            }while(true);
            deliveryPoint = stores.get(indexMapings.get(storeChoice));
            
            validationSet.clear();
            indexMapings.clear();
            storeChoice = -1;
            System.out.format(stores.get(0).printHeader());
            for(int i = 0 ; i < stores.size(); i++){
                if(!stores.get(i).getType().equals(deliveryPoint.getType()))
                    continue;
                System.out.print(stores.get(i).toString());
                validationSet.add(stores.get(i).getId());
                indexMapings.put(stores.get(i).getId(), i);
            }
            System.out.format(stores.get(0).printFooter());
            do{
                try{
                    System.out.print("Select return point from the list above(ID)? ");
                    storeChoice =  Integer.parseInt(scanner.nextLine());
                }catch(NumberFormatException e){
                    storeChoice = -1;
                }
                if(validationSet.contains(storeChoice))
                    break;
            }while(true);
            
            returnPoint = stores.get(indexMapings.get(storeChoice));
            System.out.println();
            System.out.println("Enter user information");
            System.out.println("____________________________");
            User user = getUserInformation(deliveryPoint);
            userRegistry.put(user.getAMKA(), user);
            ++RENTAL_RESERVATION;
            String paymentMethod = (user.getCreditCardNumber() == null) ? RentalReservation.CASH : RentalReservation.CREDIT_CARD;
            return new RentalReservation(RENTAL_RESERVATION, rentDate, rentHour, returnDate, returnHour, paymentMethod, vehicle, user, deliveryPoint, returnPoint);
        }
        System.out.println("There are no available vehicles");
        return null;
    }
    /**
     * A method for updating the state of rental reservation object.
     * A menu with six options is printed to the user.
     * Then the user is prompted select an option.
     * This method can:
     * 	1) Update the rent date or hour of a rental reservation
     * 	2) Update the return date or hour of a rental reservation
     * 	3) Cancel a rental reservation
     * 	4) Update the pick-up point
     * 	5) Update the return point
     * When the user has selected an option this method invokes the appropriate method to provide the service requested by the user.
     */
    private void updateRentalReservation(){
        if(RENTAL_RESERVATION > 0){
            System.out.println("Update a rental reservation");
            int choice = 0;
            boolean run = true;
            RentalReservation rentalReservation;
            do{
                System.out.print("Rental reservation unique id? ");
                try{
                    rentalReservation = rentalReservationsRegistry.get(Integer.parseInt(scanner.nextLine()));
                }catch(NumberFormatException e){
                    rentalReservation = null;
                }
            }while( rentalReservation == null );
            do{
                rentalReservation.printRentalReservation();
                System.out.println("1: Update rent date or hour");
                System.out.println("2: Update return date or hour");
                System.out.println("3: Cancel rental reservation");
                System.out.println("4: Update pick-up point");
                System.out.println("5: Update return point");
                System.out.println("6: Return to main menu");
                do{
                    try{
                        System.out.print("Select an option from the menu above? ");
                        choice = Integer.parseInt(scanner.nextLine());
                    }catch(NumberFormatException e){
                        choice = 0;
                    }
                }while(choice < 0 || choice > 6);
                
                switch(choice){
                    case 1:
                        boolean[] updatedRentDateTimeFields = updateRentDateTime(rentalReservation);
                        System.out.print( (updatedRentDateTimeFields[0] ) ? ("**Rent date updated: " + rentalReservation.getRentDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy") ) +"\n"): "");
                        System.out.print( (updatedRentDateTimeFields[1] ) ? ("*Rent hour updated: " + rentalReservation.getRentDateTime().format(DateTimeFormatter.ofPattern("HH:mm") ) +"\n"): "");
                        break;
                        
                    case 2:
                        boolean[] updatedReturnDateTimeFields = updateReturnDateTime(rentalReservation);
                        System.out.print( (updatedReturnDateTimeFields[0] ) ? ("**Return date updated: " + rentalReservation.getReturnDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy") ) +"\n"): "");
                        System.out.print( (updatedReturnDateTimeFields[1] ) ? ("**Return hours updated: " + rentalReservation.getReturnDateTime().format(DateTimeFormatter.ofPattern("HH:mm") ) +"\n"): "");
                        break;
                        
                    case 3:
                        cancelRentalReservation(rentalReservation);
                        run = false;
                        break;
                    case 4:
                        boolean[] updatePickupPoint = updatePickupPoint(rentalReservation);
                        if(updatePickupPoint[0])
                            rentalReservation.getDeliveryPoint().printStoreDetailsUpdated();
                        if(updatePickupPoint[1])
                            rentalReservation.getReturnPoint().printStoreDetailsUpdated();
                        break;
                        
                    case 5:
                        boolean updateReturnPoint = updateReturnPoint(rentalReservation);
                        if(updateReturnPoint)
                            rentalReservation.getReturnPoint().printStoreDetailsUpdated();
                        break;
                        
                    case 6:
                        run = false;
                    default:
                        break;
                }
            }while(run);
        }
        else
            System.out.println("There are no rental reservations in the system!");
    }
   /**
     * A method responsible for printing the details of the user's rental reservations.
     * This method, when called, prompts the user to give his AMKA as input for the system 
     * and then the system checks at the userRentalReservations "dictionary" 
     * for the users AMKA.
     * If AMKA exists, then a list containing all the RentalReservation objects of the user 
     * is returned, otherwise an appropriate message informs the user that the AMKA entered doesn't exist
     * in the system.
     */ 
    private void viewRentalReservationDetails(){
        System.out.print("Please enter your amka? ");
        String AMKA = scanner.nextLine();
        List<RentalReservation> rentalReservations = userRentalReservations.get(AMKA);
        if( rentalReservations == null)
            System.out.println("No user with AMKA: " + AMKA + " found");
        else
        {
            System.out.printf("+--------+------------+------------------+--------------+------------+---------------+------------------------+------------------+--------------+--------------------------+--------------------------+------------------------+--------------------+--------------------+------------------------+------------------------+\n");
            System.out.printf("| ID     | Cost       | Payment Method   | Vehicle Type | Fuel       | Horsepower    | Engine Displacement    | Luggage Space    | AMKA         | First Name               | Last Name                | Credit card            | Rent date          | Return date        | Pick-up point address  | Return point address   |\n");
            System.out.printf("+--------+------------+------------------+--------------+------------+---------------+------------------------+------------------+--------------+--------------------------+--------------------------+------------------------+--------------------+--------------------+------------------------+------------------------+\n");
            Car car;
            for(RentalReservation rentalReservation : userRentalReservations.get(AMKA)){
                if(rentalReservation.getVehicle() instanceof Car){
                    car = (Car)rentalReservation.getVehicle();
                    System.out.printf("| %-6d |\u20AC%10.2f | %-16s | %-12s | %-10s | %-13.2f | %-22.2f | %-2d               | %-11s  | %-24s | %-24s | %-22s | %-18s | %-18s | %-22s | %-22s |%n", rentalReservation.getId(),rentalReservation.getCost(), rentalReservation.getPaymentMethod(), "Car", rentalReservation.getVehicle().getFuel(), rentalReservation.getVehicle().getHorsepower(), rentalReservation.getVehicle().getEngineDiscplacement(), car.getLuggageSpace(), rentalReservation.getUser().getAMKA(), rentalReservation.getUser().getFirstName(), rentalReservation.getUser().getLastName(), rentalReservation.getUser().getCreditCardNumber(), rentalReservation.getRentDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")), rentalReservation.getReturnDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")), rentalReservation.getDeliveryPoint().getAddress().getStreetAddress(), rentalReservation.getReturnPoint().getAddress().getStreetAddress());
                }
                else if(rentalReservation.getVehicle() instanceof TwoWheeler){
                    System.out.printf("| %-6d |\u20AC%10.2f | %-16s | %-12s | %-10s | %-13.2f | %-22.2f | %-2d               | %-11s  | %-24s | %-24s | %-22s | %-18s | %-18s | %-22s | %-22s |%n", rentalReservation.getId(),rentalReservation.getCost(), rentalReservation.getPaymentMethod(), "Two wheeler", rentalReservation.getVehicle().getFuel(), rentalReservation.getVehicle().getHorsepower(), rentalReservation.getVehicle().getEngineDiscplacement(), 0, rentalReservation.getUser().getAMKA(), rentalReservation.getUser().getFirstName(), rentalReservation.getUser().getLastName(), rentalReservation.getUser().getCreditCardNumber(), rentalReservation.getRentDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")), rentalReservation.getReturnDateTime().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")), rentalReservation.getDeliveryPoint().getAddress().getStreetAddress(), rentalReservation.getReturnPoint().getAddress().getStreetAddress());
                    
                }
            }
            System.out.printf("+--------+------------+------------------+--------------+------------+---------------+------------------------+------------------+--------------+--------------------------+--------------------------+------------------------+--------------------+--------------------+------------------------+------------------------+\n");
        }
    }
   /**
     * validRentHours method validates the rent hours that the user has provided as input in the system.
     * $When this method is called the rent date has been already validated.
     * In order for the rent time to be valid, this method checks if the rent date is the the today's date:
     * 	  If the rent date matches the today's date then the system checks if the rent hour is later than the current hour and not earlier:
     * 		If current hour are at least one minute earlier of the rent hour then the rent hour is valid. 
     * 		If current hour is equal or greater than the rent hour then the rent hour is invalid (e.g the user
     * 		cannot give rent hour earlier than the current hour).
     *    If the rent date is one or more days in the future then rent hours are valid. 
     * @param rentDate A string with the rent date of a vehicle.
     * @param rentHours A string with the rent hours.
     * @return a value indicating if the rent date and time strings that the user has entered are valid.
     */
    private boolean validRentHours(String rentDate, String rentHour){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate rentDateTime = LocalDate.parse(rentDate, dateTimeFormatter);
        LocalDate today = LocalDate.now();
        Period period = Period.between(today, rentDateTime);
        
        if( period.getDays() == 0 && period.getMonths() == 0){
            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
            LocalTime rentTime = LocalTime.parse(rentHour, timeFormatter);
            LocalTime now = LocalTime.now();
            //You cannot rent before the current time
            long diff = now.until(rentTime, MINUTES);
            if( diff < 0){
                System.out.println("You cannot rent before the current time");
                return false;
            }
            return true;
        }
        else if(period.getDays() > 0 || period.getMonths() > 0)
            return true;
        else{
            System.out.println("Fatal error, please contact with the admin!");
            System.err.println("Fatal error, please contact with the admin!");
            return true;
        }
    }
    /**
     * $When validReturnHours method is invoked rentDate, returnDate, rentHours will have already been validated.
     * Valid return hour means that the return hour should be at least one hour later than the rent hour. This means
     * that in order to rent a car someone will have to rent it at least for one hour. 
     * @param rentDate The rent date of the vehicle
     * @param returnDate The return date of the vehicle
     * @param rentHour The rent hour of the vehicle
     * @param returnHour The return hour of the vehicle
     * @return a values indicating whether the return hour are valid or not.
     */
    private boolean validReturnHours(String rentDate, String returnDate, String rentHour, String returnHour){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate rentDateTime = LocalDate.parse(rentDate, dateTimeFormatter);
        LocalDate returnDateTime = LocalDate.parse(returnDate, dateTimeFormatter);
        Period period = Period.between(rentDateTime, returnDateTime);
        
        if( period.getDays() == 0 && period.getMonths() == 0){
            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
            LocalTime rentTime = LocalTime.parse(rentHour, timeFormatter);
            LocalTime returnTime = LocalTime.parse(returnHour, timeFormatter);
            //AT LEAST ONE HOUR OF RENT
            long diff = rentTime.until(returnTime, HOURS);
            if( diff < 1){
                System.out.println("You should at least rent the car for one hour");
                return false;
            }
            else return true;
        }
        else if( period.getMonths() > 0 || period.getDays() > 0 ){
            return true;
        }
        else
        {
            System.out.println("Fatal error, please contact with the admin!");
            System.err.println("Fatal error, please contact with the admin!");
            return true;
        }
        
    }
    /**
     * $When validReturnDate method is invoked rentDate parameter will have already been validated.
     * Valid return date means that the return date of a vehicle cannot be earlier than the rent date.
     * For example give that rent date is 17/01/2020 a valid return date could be 18/01/2020.
     * If rent date was 17/01/2020 and return date was 17/01/2020, then this return date would be valid.
     * Invalid will be a return date, if 17/01/2020 is the rent date and the return date is 16/01/2020 
     * @param rentDate The rent date of a vehicle.
     * @param returnDate The return date of a vehicle.
     * @return whether the return date is valid or not.
     */
    private boolean validReturnDate(String rentDate, String returnDate){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate rentDateTime = LocalDate.parse(rentDate, dateTimeFormatter);
        LocalDate returnDateTime = LocalDate.parse(returnDate, dateTimeFormatter);
        Period period = Period.between(rentDateTime, returnDateTime);
        
        if( period.getYears() == 0 ){
            if(period.getMonths() >= 0 && period.getDays() >= 0)
                return true;
            else{
                System.out.println("Return date: " + returnDate + " should be equal or greater than rent date: " + rentDate);
                return false;
            }
        } else {
            System.out.println("You can only make rental reservations within one year from today"); //between(17/12/2019, 17/12/2020) returns 1 between(16/12/2019, 17/12/2020) returns 0
            return false;
        }
    }
    /**
     * validRentDate method checks if the time period of the rent date doesn't exceed one year from today.
     * @param rentDate The rent date of a vehicle.
     * @return If the rent date is valid or not
     */
    private boolean validRentDate(String rentDate){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate now = LocalDate.now();
        LocalDate rentDateTime = LocalDate.parse(rentDate, dateTimeFormatter);
        Period period = Period.between(now, rentDateTime);
        
        if( period.getYears() == 0 && period.getMonths() >= 0 && period.getDays() >= 0) 
            return true;
        else{
            System.out.println("You can only make rental reservations from one year from today"); //between(17/12/2019, 17/12/2020) returns 1 between(16/12/2019, 17/12/2020) returns 0
            return false;
        }
    }
    /**
     * A method for getting the AMKA, first name, last name, identity number, phone number, drivers license number from a user.
     * Appropriate messages are printed to the console.
     * User is asked to provide input to the system.
     * User's input is validated through regular expressions to prevent the system from crashing.
     * @param deliveryPoint
     * @return a User object containing all the information the user has entered. 
     */
    private User getUserInformation(Store deliveryPoint){
        Pattern AMKAPattern = Pattern.compile("[0-9]{11}");
        Pattern identityPattern = Pattern.compile(".{1,2} [0-9]{6}");
        Pattern phoneNumberPattern = Pattern.compile("^69[0-9]{8}");
        Pattern driversLicensePattern = Pattern.compile("[0-9]{3,10}");
        
        String AMKA;
        String firstName;
        String lastName;
        String identityNumber;
        String phoneNumber;
        String driversLicenceNumber;
        
        do{
            System.out.print("AMKA? ");
            AMKA = scanner.nextLine();
        }while( !AMKAPattern.matcher(AMKA).matches() );
        if(!userRegistry.containsKey(AMKA)){
            do{
                System.out.print("First name? ");
                firstName = scanner.nextLine();
            }while(!firstName.matches("(?i)(^[a-z])((?![ .,'-]$)[a-z .,'-]){0,24}$" ) && !firstName.matches("(?i)(^[ά-ώ])((?![ .,'-]$)[ά-ώ .,'-]){0,24}$" ));
            do{
                System.out.print("Last name? ");
                lastName = scanner.nextLine();
            }while(!lastName.matches("(?i)(^[a-z])((?![ .,'-]$)[a-z .,'-]){0,24}$" ) && !lastName.matches("(?i)(^[ά-ώ])((?![ .,'-]$)[ά-ώ .,'-]){0,24}$" ));
            do{
                System.out.print("Identity number? ");
                identityNumber = scanner.nextLine();
            }while(!identityPattern.matcher(identityNumber).matches());
            do{
                System.out.print("Drivers licence number? ");
                driversLicenceNumber = scanner.nextLine();
            }while(!driversLicensePattern.matcher(driversLicenceNumber).matches());
            int age = 0;
            do{
                System.out.print("Age? ");
                try{
                    age = Integer.parseInt(scanner.nextLine());
                }catch(NumberFormatException e){
                    System.out.println("Please enter valid age!");
                    age = 0;
                }
            } while( age < 18 );
            do{
                System.out.print("Phone number? ");
                phoneNumber = scanner.nextLine();
            }while(!phoneNumberPattern.matcher(phoneNumber).matches());
            String creditCardNumber = getCreditCardInformation(deliveryPoint);
            ++USER_COUNTER;
            return new User(USER_COUNTER, AMKA, firstName, lastName, identityNumber, driversLicenceNumber ,age, phoneNumber, creditCardNumber);
        } else {
            System.out.println("User with AMKA: " + AMKA + " already exists in the system");
            userRegistry.get(AMKA).setCreditCardNumber(getCreditCardInformation(deliveryPoint));
            return userRegistry.get(AMKA);
        }
    }
    /**
     * A user can have many rental reservations and one rental reservation belongs only to one user(N:1).
     * Double brace initialization method is used(https://stackoverflow.com/questions/16194921/initializing-arraylist-with-some-predefined-values).
     * This method checks if the user's AMKA(unique identifier) exists in the userRentalReservations "dictionary".
     *  1) If it doesn't exist it creates a new entry. The key of the "dictionary" will be the AMKA and the value will be an ArrayList with one element (the rentalReservation parameter).
     *  2) If it exists, it gets the user's rental reservations  list  and appends the specified in the method parameters.  
     * @param userRentalReservations A "dictionary" containing mappings between the user's AMKA and the user's rental reservation list. 
     * @param rentalReservation A rental reservation of a user that will append on the user's rental reservation list.
     */
    private void assignRentalReservationsToUser(Map<String, List<RentalReservation>> userRentalReservations, RentalReservation rentalReservation){
        if ( userRentalReservations.get(rentalReservation.getUser().getAMKA()) == null ){
            userRentalReservations.put(rentalReservation.getUser().getAMKA(),
                    new ArrayList<RentalReservation>()
                    {{
                        add(rentalReservation);
                    }}
            );
        } else {
            userRentalReservations.get( rentalReservation.getUser().getAMKA() ).add(rentalReservation);
        }
    }
    /**
     * updatedRentDateTime method receives as input a RentalReservation objects and updates rentDate, rentTime or both of them returning to the caller method
     * information about which fields have been updated.
     * @param rentalReservation A rentalReservation object to be updated.
     * @return an array containing two values. if the first values is true, rent date has been updated.
     * If second value is true, rent hour has been updated. If both are true, then both the rent date and hour have been updated.
     */
    private boolean[] updateRentDateTime(RentalReservation rentalReservation){
        LocalDateTime rentDateTime = rentalReservation.getRentDateTime();
        boolean[] updatedFields = { false, false };
        boolean run = true;
        if( updateAvailable(rentDateTime ) ){
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            DateTimeFormatter dttf = DateTimeFormatter.ofPattern("HH:mm");
            
            String newRentDate;
            String newRentHour;
            int choice = -1;
            do{
                rentDateTime = rentalReservation.getRentDateTime();
                System.out.println("Current rent date is set at " + rentDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
                System.out.println("1) Update rent date");
                System.out.println("2) Update rent hours");
                System.out.println("3) Exit");
                do{
                    System.out.print("Select an option from the menu above? ");
                    try{
                        choice = Integer.parseInt(scanner.nextLine());
                    }catch(NumberFormatException e){
                        choice = -1;
                    }
                } while( choice < 1 && choice > 3 );
                switch(choice){
                    case 1:
                        if( penaltyChecker(rentDateTime) != null)
                            System.out.println(penaltyChecker(rentDateTime));
                        do{
                            newRentDate = getUpdatedRentDate(rentalReservation);
                            rentalReservation.setRentDateTime(newRentDate, rentalReservation.getRentDateTime().format(dttf));
                        }while(!updateAvailable(rentalReservation.getRentDateTime()));
                        if(imposePenalty(rentDateTime, rentalReservation.getRentDateTime()))
                            System.out.println("You will have to pay " + (rentalReservation.getCost()*0.3) + " as penalty for updating the rental reservation in less than 48 hours");
                        updatedFields[0] = true;
                        break;
                    case 2:
                        String curRentDate = dtf.format(rentalReservation.getRentDateTime());
                        newRentHour = getRentHours(curRentDate);
                        rentalReservation.setRentDateTime(curRentDate, newRentHour);
                        updatedFields[1] = true;
                        break;
                    case 3:
                        run = false;
                        break;
                    default:
                        System.out.println("Incorect choice");
                }
                
            }while(run);
            if( updatedFields[0] == true || updatedFields[1] == true)
                rentalReservation.calculateRentalReservationCost();
            return updatedFields;
        }
        
        return updatedFields;
    }
   /**
     * updatedReturnDateTime method receives as input a RentalReservation objects and updates returnDate, returnTime or both of them returning to the caller method
     * information about which fields have been updated.
     * @param rentalReservation A rentalReservation object to be updated.
     * @return an array containing two values. if the first values is true, return date has been updated.
     * If second value is true, return hour has been updated. If both are true, then both the rent date and hour have been updated.
     */
    private boolean[] updateReturnDateTime(RentalReservation rentalReservation) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter dttf = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime returnDateTime;
        String currentRentDate = rentalReservation.getRentDateTime().format(dtf);
        String currentRentHours = rentalReservation.getRentDateTime().format(dttf);
        
        boolean[] updatedFields = { false, false };
        String newReturnDate = null;
        String newReturnHour = null;
        
        boolean run = true;
        int choice = -1;
        do{
            returnDateTime = rentalReservation.getReturnDateTime();
            System.out.println("Current return date is set at " + returnDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
            System.out.println("1) Update return date");
            System.out.println("2) Update return hours");
            System.out.println("3) Exit");
            do{
                System.out.print("Select an option from the menu above? ");
                try{
                    choice = Integer.parseInt(scanner.nextLine());
                }catch(NumberFormatException e){
                    choice = -1;
                }
            } while( choice < 1 && choice > 3 );
            switch(choice){
                case 1:
                    newReturnDate = getReturnDate(currentRentDate);
                    rentalReservation.setReturnDateTime(newReturnDate, rentalReservation.getReturnDateTime().format(dttf));
                    updatedFields[0] = true;
                    break;
                case 2:
                    String curReturnDate = dtf.format(rentalReservation.getReturnDateTime());
                    newReturnHour = getReturnHour(currentRentDate, curReturnDate, currentRentHours);
                    rentalReservation.setReturnDateTime(curReturnDate, newReturnHour);
                    updatedFields[1] = true;
                    break;
                case 3:
                    run = false;
                    break;
                default:
                    System.out.println("Invalid input!");
            }
        }while(run);
        if( updatedFields[0] == true || updatedFields[1] == true)
            rentalReservation.calculateRentalReservationCost();
        return updatedFields;
    }
    /**
     * This method cancels a rental reservation by removing the rental reservation object from the system.
     * It prints a message if it is required for the user to pay penalty according to the company's policies.
     * @param rentalReservation The rental reservation that will be removed.
     */
    private void cancelRentalReservation(RentalReservation rentalReservation) {
        if( imposePenalty(rentalReservation) )
            System.out.println("You will have to pay " + ( rentalReservation.getCost() * 0.3 ) + " as penalty for updating the rental reservation in less than 48 hours");
        rentalReservation.returnVehicle();
        rentalReservationsRegistry.remove(rentalReservation.getId());
        userRentalReservations.get(rentalReservation.getUser().getAMKA()).remove(rentalReservation);
        System.out.println("Rental reservation with id: " + rentalReservation.getId() + " has been cancelled");
    }
    /**
     * 
     * @param rentalReservation The rental reservation that the pick up point will be changed.
     * @return An with two values. If first values is true then pick up point has updated.
     *  Because pick-up point and delivery point must be of the same type if second value is true
     * this means the delivery point has also updated to match store type with the pick-up point.
     */
    private boolean[] updatePickupPoint(RentalReservation rentalReservation) {
        rentalReservation.getDeliveryPoint().printStoreDetails();
        Set<Integer> validationSet = new HashSet<>();
        Map<Integer, Integer> indexMappings = new HashMap<>();
        int storeChoice = -1;
        Store deliveryPoint;
        char answer;
        boolean[] updatedFields = { false, false};
        do{
            System.out.print("Would you like to update the current delivery point ( y for yes or n for no)? ");
            answer = scanner.nextLine().charAt(0);
        } while( answer != 'y' && answer != 'n' );
        
        if( answer == 'y' ){
            validationSet.clear();
            indexMappings.clear();
            System.out.format(stores.get(0).printHeader());
            for(int i = 0 ; i < stores.size(); i++){
                System.out.print(stores.get(i).toString());
                validationSet.add(stores.get(i).getId());
                indexMappings.put(stores.get(i).getId(), i);
            }
            System.out.format(stores.get(0).printFooter());
            do{
                try{
                    System.out.print("Select a delivery point from the table above(ID)? ");
                    storeChoice = Integer.parseInt(scanner.nextLine());
                }catch(NumberFormatException e){
                    storeChoice = -1;
                }
                if(validationSet.contains( storeChoice ))
                    break;
            }while(true);
            deliveryPoint = stores.get( indexMappings.get(storeChoice) );
            rentalReservation.setDeliveryPoint(deliveryPoint);
            updatedFields[0] = true;
            if( !rentalReservation.getDeliveryPoint().getType().equals(rentalReservation.getReturnPoint().getType() ) ){
                System.out.println("Because deivery point and return point should be of the same type please select a valid return point from the menu");
                validationSet.clear();
                indexMappings.clear();
                System.out.format(stores.get(0).printHeader());
                for( int i = 0 ; i < stores.size(); i++){
                    if(!stores.get(i).getType().equals(rentalReservation.getDeliveryPoint().getType()))
                        continue;
                    System.out.print(stores.get(i).toString());
                    validationSet.add(stores.get(i).getId());
                    indexMappings.put(stores.get(i).getId(), i);
                }
                System.out.format(stores.get(0).printFooter());
                do{
                    try{
                        System.out.print("Select return point from the table above(ID)? ");
                        storeChoice =  Integer.parseInt(scanner.nextLine());
                    }catch(NumberFormatException e){
                        storeChoice = -1;
                    }
                    if(validationSet.contains(storeChoice))
                        break;
                }while( true );
                rentalReservation.setReturnPoint(stores.get(indexMappings.get(storeChoice)));
                updatedFields[1] = true;
            }
        }
        return updatedFields;
    }
    /**
     * 
     * @param rentalReservation The rental reservation that the return point will be updated.
     * @return a value to the caller method, indicating whether the return point has been updated or not.
     */
    private boolean updateReturnPoint(RentalReservation rentalReservation) {
        rentalReservation.getReturnPoint().printStoreDetails();
        Set<Integer> validationSet = new HashSet<>();
        Map<Integer, Integer> indexMappings = new HashMap<>();
        int storeChoice = -1;
        Store returnPoint;
        char answer;
        do{
            System.out.print("Would you like to update the current return point ( y for yes or n for no)? ");
            answer = scanner.nextLine().charAt(0);
        } while( answer != 'y' && answer != 'n' );
        
        if( answer == 'y' ){
            System.out.format(stores.get(0).printHeader());
            for( int i = 0 ; i < stores.size(); i++){
                if(!stores.get(i).getType().equals(rentalReservation.getReturnPoint().getType()))
                    continue;
                System.out.print(stores.get(i).toString());
                validationSet.add(stores.get(i).getId());
                indexMappings.put(stores.get(i).getId(), i);
            }
            System.out.format(stores.get(0).printFooter());
            do{
                try{
                    System.out.print("Select return point from the table above(ID)? ");
                    storeChoice =  Integer.parseInt(scanner.nextLine());
                }catch(NumberFormatException e){
                    storeChoice = -1;
                }
                if(validationSet.contains( storeChoice) )
                    break;
            }while( true );
            rentalReservation.setReturnPoint(stores.get(indexMappings.get(storeChoice)));
            return true;
        }
        return false;
    }
    /**
     * This method prompts the user to enter rent date.
     * User's input is then validated.
     * A valid rent date must follow the specified format(dd/MM/yyyy)
     * If the rent date the user entered is invalid the procedure is repeated again.
     * @return A valid rent date string.
     */
    private String getRentDate( ) {
        String rentDate;
        do{
            do{
                System.out.print("Enter rent date (dd/mm/yyyy), example 01/05/2019, leading zeros required? ");
                rentDate = scanner.nextLine();
                if(datePattern.matcher(rentDate).matches())
                    break;
            }while(true);
        }while(!validRentDate(rentDate));
        return rentDate;
    }
    /**
     * This method prompts the user to enter rent date.
     * User's input is then validated.
     * A valid rent date must follow the specified format(dd/MM/yyyy)
     * If the rent date the user entered is invalid the procedure is repeated again.
     * @param rentalReservation A rental reservation object to set the new rent date.
     * @return A valid rent date string.
     */
    private String getUpdatedRentDate(RentalReservation rentalReservation){
        String rentDate;
        do{
            do{
                do{
                    System.out.print("Enter the new rent date (dd/mm/yyyy), example 01/05/2019, leading zeros required, at least 24 hours ahead from today? ");
                    rentDate = scanner.nextLine();
                    if(datePattern.matcher(rentDate).matches())
                        break;
                }while(true);
            }while(!validRentDate(rentDate));
            rentalReservation.setRentDateTime(rentDate, rentalReservation.getRentDateTime().format(DateTimeFormatter.ofPattern("HH:mm")));
        }while(!validRentDate(rentalReservation));
        return rentDate;
    }
   /**
     * This method prompts the user to enter rent hours.
     * User's input is then validated.
     * A valid rent hour must follow the specified format(hh:mm).
     * An invocation to the validRentHours method is followed to check further the validity of rent hours.
     * If the rent hour the user entered is invalid the procedure is repeated again.
     * @return A valid rent hour string.
     */
    private String getRentHours(String rentDate ) {
        String rentHour;
        do{
            do{
                System.out.print("Enter hours and minutes (hh:mm) in 24 hour format only, example 00:00, 13:01, 23:10, 02:00? ");
                rentHour = scanner.nextLine();
                if(hoursPattern.matcher(rentHour).matches())
                    break;
            }while(true);
        }while(!validRentHours(rentDate, rentHour));
        return rentHour;
    }
    /**
     * This method prompts the user to enter return date.
     * User's input is then validated.
     * A valid return date must follow the specified format(dd/MM/yyyy)
     * If the return date follows the specified format then an invocation to the validReturnDate method is done in order to check the validity 
     * of the return date according to the rent date.
     * If the return date the user entered is invalid the procedure is repeated again.
     * @param rentDate The vehicle's rent date.
     * @return A valid rent date string.
     */
    private String getReturnDate(String rentDate) {
        String returnDate;
        do{
            do{
                System.out.print("Enter return date (dd/mm/yyyy), example 01/05/2019, leading zeros required? ");
                returnDate = scanner.nextLine();
                if(datePattern.matcher(returnDate).matches())
                    break;
            }while(true);
        }while(!validReturnDate(rentDate, returnDate));
        return returnDate;
    }
    /**
     * getRentHour method prompts the user to enter return hour for the vehicle.
     * User's input is first validate regarding to its format. Valid return hour format hh:mm.
     * Then an invocation to validReturnHours method ensures that return hour is valid regarding the rules described in validReturnHours method.
     * @param rentDate The rent date of a vehicle.
     * @param returnDate The return date of a vehicle.
     * @param rentHour The return hour the user has entered for the rental of a vehicle.
     * @return A valid return hour string.
     */
    private String getReturnHour(String rentDate, String returnDate, String rentHour) {
        String  returnHour;
        do{
            do{
                System.out.print("Enter hours and minutes (hh:mm) in 24 hour format only, example 00:00, 13:01, 23:10, 02:00? ");
                returnHour = scanner.nextLine();
                if(hoursPattern.matcher(returnHour).matches())
                    break;
            }while(true);
        }while(!validReturnHours(rentDate, returnDate, rentHour, returnHour));
        return returnHour;
    }
    
    public void close(){
        scanner.close();
    }
    
    /**
     * updateAvailable method ensures that a user can update a rental reservation if only the rental reservation is schedule from more than 24 hours from now.
     * It is also used to ensure that a user cannot update the rent date and set it 24 hours from now.
     * @param rentDateTime
     * @return a value indicating whether a user can update a rental reservation or not.
     * 
     */
    private boolean updateAvailable(LocalDateTime rentDateTime) {
        return  LocalDateTime.now().until(rentDateTime, HOURS) >= 24;
    }
    /**
     * This method is used to ensure that a user will not intentionally update the rent date and time to avoid getting a penalty by cancelling a rental reservation.
     * @param rentDateTime The current rent date of the vehicle.
     * @param newRentDateTime The new rent date of the vehicle that the user entered to the update method.
     * @return a value indicating whether updating a rent date that was about to start in less than 48 hours and setting it to something greater than 48 hours will impose money penalty or not.
     */
    private boolean imposePenalty(LocalDateTime rentDateTime, LocalDateTime newRentDateTime){
        LocalDateTime now = LocalDateTime.now();
        long hours =  now.until(rentDateTime, HOURS);
        
        
        if( hours >= 24 && hours < 48 ){
            //48 - hours -> no penalty
            if( now.until(newRentDateTime, HOURS) > 47 )
                return true;
        }
        return false;
    }
    /**
     * @param rentalReservation a rental reservation object to be cancelled.
     * @return a value indicating that cancelling only 48 hours earlier than the rent date time the rental reservation a penalty should be imposed or not.
     */
    private boolean imposePenalty(RentalReservation rentalReservation){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime rentDateTime = rentalReservation.getRentDateTime();
        int diffHours = ( int ) now.until(rentDateTime, HOURS);
        return (diffHours < 48);
    }
    /**
     * This method, informs the user, the time period that a penalty is not imposed, if the rent date and time is set to that time period.This is true only when the rent date was about to start in less than 48 hours.
     * If the rent date and was about to start in more than 48 hours then no penalty is imposed.
     * @param rentDateTime The rent date time of a rental reservation.
     * @return a string that will inform the user the time period that a penalty will no imposed, if it was to imposed(>48 hours no penalty).
     */
    private String penaltyChecker(LocalDateTime rentDateTime){
        LocalDateTime now = LocalDateTime.now();
        long diff =  now.until(rentDateTime, MINUTES);
        
        int hours = (int) diff / 60;
        int minutes = (int) diff % 60;
        
        if( hours >= 24 && hours < 48 ){
            return "You can shift " + (47 -hours) + " hours and " + ( 59 - minutes) + " minutes the rental reservation without getting a penalty";
        }
        return null;
    }
    /**
     * A rent date is valid if it doesn't exceed the return date (e.g rent date 17/01/2020, return date 18/01/2020 -> valid). 
     * @param rentalReservation A rental reservation object.
     * @return whether the rent date string is valid or not.
     */
    private boolean validRentDate(RentalReservation rentalReservation) {
        LocalDateTime rentDateTime = rentalReservation.getRentDateTime();
        LocalDateTime returnDateTime = rentalReservation.getReturnDateTime();
        if( rentDateTime.until(returnDateTime, MINUTES) <= 0 ){
            System.out.println("Rent date time should be smaller than return date time!");
            return false;
        }
        return true;
    }
    /**
     * According to the type of the store this method prompts the user to select payment method.
     * If the type of the store allows credit card then a menu is presented to the user to select paymnent method.
     * Validation to the user's input is done.
     * @param deliveryPoint A store object containing.
     * @return a valid credit card number.
     */
    private String getCreditCardInformation(Store deliveryPoint) {
        
        String creditCardNumber;
        Pattern creditCardPattern = Pattern.compile("[0-9]{4} [0-9]{4} [0-9]{4} [0-9]{4}");
        if(deliveryPoint.getType().equals(Store.SIMPLE_STORE)){
            
            System.out.println("1) Cash");
            System.out.println("2) Credit card");
            int paymentMethod = 0;
            do{
                System.out.print("Select payment method? ");
                try{
                    paymentMethod = Integer.parseInt(scanner.nextLine());
                }catch(NumberFormatException e){
                    paymentMethod = 0;
                }
            } while( paymentMethod != 1 && paymentMethod != 2 );
            
            if(paymentMethod == 1)
                return null;
            else{
                do{
                    System.out.print("Credit card number? ");
                    creditCardNumber = scanner.nextLine();
                }while(!creditCardPattern.matcher(creditCardNumber).matches());
                return creditCardNumber;
            }
        }
        else
            return null;
    }
    
}