
package it21622;

/**
 * A subclass that extends Vehicle class. This class contains some additional fields required for our use case.
 * An object of Car class represents a Car of the JAVIS shop, rent by customers.
 * @author it21622
 */
public class Car extends Vehicle {
    private int seats;
    private int doors;
    private int luggageSpace;

    public Car(int id, String fuel, double horsepower, double wheelSize, double engineDiscplacement, double cost, int seats, int doors, int luggageSpace) {
        super(id, fuel, horsepower, wheelSize, engineDiscplacement, cost);
        this.seats = seats;
        this.doors = doors;
        this.luggageSpace = luggageSpace;
    }

    public int getSeats() {
        return seats;
    }

    public int getDoors() {
        return doors;
    }

    public int getLuggageSpace() {
        return luggageSpace;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public void setLuaggageSpace(int luaggageSpace) {
        this.luggageSpace = luaggageSpace;
    }

    @Override
    public String toString() {
        return String.format("| %-6d | %-16s | %-14.2f | %-22.2f | %-14.2f |\u20AC%-10.2f | %-6d | %-6d | %-14d |%n", getId(),getFuel(), getHorsepower(), getEngineDiscplacement(), getWheelSize(), getCost(), seats, doors, luggageSpace);
    }

    /**
     * A method that prints the details of a car formatted.
     */
    @Override
    public void printVehicleDetails() {
        System.out.printf("+--------+------------------+----------------+------------------------+----------------+------------+--------+--------+----------------+%n");
        System.out.printf("| ID     | Fuel             | Horsepower     | Engine Displacement    | Wheel Size     | Cost       | Seats  | Doors  | Luggage space  |%n");
        System.out.printf("+--------+------------------+----------------+------------------------+----------------+------------+--------+--------+----------------+%n");
        System.out.printf("| %-6d | %-16s | %-14.2f | %-22.2f | %-14.2f |\u20AC%-10.2f | %-6d | %-6d  | %-14d |%n", getId(),getFuel(), getHorsepower(), getEngineDiscplacement(), getWheelSize(), getCost(), seats, doors, luggageSpace);
        System.out.printf("+--------+------------------+----------------+------------------------+----------------+------------+--------+--------+----------------+%n");
    }
    /**
     * A method that prints a formatted string to the screen.
     * @return a formatted string.
     */
    @Override
    public String printHeader() {
       return "+--------+------------------+----------------+------------------------+----------------+------------+--------+--------+----------------+%n"
               + "| ID     | Fuel             | Horsepower     | Engine Displacement    | Wheel Size     | Cost       | Seats  | Doors  | Luggage space  |%n"
               + "+--------+------------------+----------------+------------------------+----------------+------------+--------+--------+----------------+%n";
    }
   /**
     * A method that prints a formatted string to the screen.
     * @return a formatted string.
     */
    @Override
    public String printFooter() {
        return "+--------+------------------+----------------+------------------------+----------------+------------+--------+--------+----------------+%n";
    }


 
}
