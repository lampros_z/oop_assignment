
package it21622;

/**
 * A subclass that extends Vehicle class. Two extra fields required for a Two wheeler object are managed by this class.
 * @author it21622
 */
public class TwoWheeler extends Vehicle {
    /**
     * https://contestimg.wish.com/api/webimage/5b174a1a1b6aee2d795f7e4f-large.jpg?cache_buster=4b0189cefb545806a6b63229c1036fb5
     */
    private boolean hasWindshield;
    /**
     * https://metalmule.com/media/wysiwyg/Motorcycle_luggage_rack.jpg
     */
    private boolean hasLuggageCarrier;
    
    public TwoWheeler(int id, String fuel, double horsepower, double wheelSize, double engineDiscplacement, double cost, boolean hasWindshield, boolean hasLuggaeCarrier) {
        super(id, fuel, horsepower, wheelSize, engineDiscplacement, cost);
        this.hasWindshield = hasWindshield;
        this.hasLuggageCarrier = hasLuggaeCarrier;
    }
    
    public boolean hasWindshield() {
        return hasWindshield;
    }
    
    public boolean hasLuggaeCarrier() {
        return hasLuggageCarrier;
    }
    
    public void setHasWindshield(boolean hasWindshield) {
        this.hasWindshield = hasWindshield;
    }
    
    public void setHasLuggageCarrier(boolean hasLuggageCarrier) {
        this.hasLuggageCarrier = hasLuggageCarrier;
    }
    
    @Override
    public String toString() {
        return String.format("| %-6d | %-16s | %-14.2f | %-22.2f | %-14.2f |\u20AC%-10.2f | %-10s | %-16s |%n", getId(),getFuel(), getHorsepower(), getEngineDiscplacement(), getWheelSize(), getCost(), hasWindshield ? "YES":"NO", hasLuggageCarrier ? "YES":"NO");
    }
   /**
     * A method that prints the details of a two-wheeler formatted.
     */
    @Override
    public void printVehicleDetails() {
        System.out.printf("+--------+------------------+----------------+------------------------+----------------+------------+------------+------------------+%n");
        System.out.printf("| ID     | Fuel             | Horsepower     | Engine Displacement    | Wheel Size     | Cost       | Windshield | Luggage Carrier  |%n");
        System.out.printf("+--------+------------------+----------------+------------------------+----------------+------------+------------+------------------+%n");
        System.out.printf("| %-6d | %-16s | %-14.2f | %-22.2f | %-14.2f |\u20AC%-10.2f | %-10s | %-16s |%n", getId(),getFuel(), getHorsepower(), getEngineDiscplacement(), getWheelSize(), getCost(), hasWindshield ? "YES":"NO", hasLuggageCarrier ? "YES":"NO");
        System.out.printf("+--------+------------------+----------------+------------------------+----------------+------------+------------+------------------+%n");
    }
    /**
     * A method that prints a formatted string to the screen.
     * @return a formatted string.
     */
    @Override
    public String printHeader() {
        return "+--------+------------------+----------------+------------------------+----------------+------------+------------+------------------+%n"
                + "| ID     | Fuel             | Horsepower     | Engine Displacement    | Wheel Size     | Cost       | Windshield | Luggage Carrier  |%n"
                + "+--------+------------------+----------------+------------------------+----------------+------------+------------+------------------+%n";
    }
    /**
     * A method that prints a formatted string to the screen.
     * @return a formatted string.
     */
    @Override
    public String printFooter() {
        return "+--------+------------------+----------------+------------------------+----------------+------------+------------+------------------+%n";
    }
    
}
