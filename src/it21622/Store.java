
package it21622;

/**
 * This class has all the necessary fields required in order to preserve the state of store Objects.
 * @author it21622
 */


public class Store {
       
    public static final String SIMPLE_STORE ="SIMPLE_STORE";
    public static final String PICKUP_POINT = "PICKUP_POINT"; 
    
    private int id;
    private String type;
    private Address address;

    public Store(int id, String  type, Address address) {
        this.id = id;
        this.type = type;
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return String.format("| %-6d | %-16s | %-26s | %-12s | %-22s | %-22s |%n", id,type, address.getStreetAddress(), address.getPostcode(), address.getTown(), address.getCountry());
    }
    
    public String printHeader(){
        return "+--------+------------------+----------------------------+--------------+------------------------+------------------------+%n"
                + "| ID     | Type             | Street Address             | Post Code    | Town                   | Country                |%n"
                + "+--------+------------------+----------------------------+--------------+------------------------+------------------------+%n";
    }
    
    public String printFooter(){
        return "+--------+------------------+----------------------------+--------------+------------------------+------------------------+%n";
    }
    public void printStoreDetails(){
        System.out.printf("+--------+------------------+----------------------------+--------------+------------------------+------------------------+%n");
        System.out.printf("| ID     | Type             | Street Address             | Post Code    | Town                   | Country                |%n");
        System.out.printf("+--------+------------------+----------------------------+--------------+------------------------+------------------------+%n");
        System.out.printf("| %-6d | %-16s | %-26s | %-12s | %-22s | %-22s |%n", id,type, address.getStreetAddress(), address.getPostcode(), address.getTown(), address.getCountry());
        System.out.printf("+--------+------------------+----------------------------+--------------+------------------------+------------------------+%n");

    }

    public void printStoreDetailsUpdated() {
        System.out.printf("+--------+------------------+----------------------------+--------------+------------------------+------------------------+%n");
        System.out.printf("| ID     | Type             | Street Address             | Post Code    | Town                   | Country                |%n");
        System.out.printf("+--------+------------------+----------------------------+--------------+------------------------+------------------------+%n");
        System.out.printf("|*%-6d | %-16s | %-26s | %-12s | %-22s | %-22s |%n", id,type, address.getStreetAddress(), address.getPostcode(), address.getTown(), address.getCountry());
        System.out.printf("+--------+------------------+----------------------------+--------------+------------------------+------------------------+%n");
        System.out.println("*UPDATED");
    }
}
