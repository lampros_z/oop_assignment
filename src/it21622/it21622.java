
package it21622;

/**
 * This class has the main method.
 * Data manager provides with data the booking system and then start method of booking system object "fires up" the system.
 * @author it21622
 * Lampros Zouloumis
 * To view rental reservation details use AMKA: 21079812345.
 * To update rental reservations you can use either id = 1 or id = 2 or create a new rental reservation.
 */
public class it21622 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        DataManager dataManager = new DataManager();
        BookingSystem bookingSystem = new BookingSystem(dataManager.getStores(), dataManager.getTwoWheelers(), dataManager.getCars(), dataManager.getRentalReservations(), dataManager.getUsers());
        bookingSystem.start();
        bookingSystem.close();
    }
    
}
